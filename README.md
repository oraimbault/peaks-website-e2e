# Peaks website end-to-end testing

## Installation

```sh
yarn install
```

**Note:** If you don't have `yarn` installed, you can also do:

```sh
npm install
```

## Launch the test environment

```sh
yarn test
```

**Note:** If you don't have `yarn` installed, you can also do:

```sh
npm run test
```

## Launch the test in a headless environment

```sh
yarn test:headless
```

**Note:** If you don't have `yarn` installed, you can also do:

```sh
npm run test:headless
```
