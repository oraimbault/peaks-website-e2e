describe("App E2E", () => {
  describe("header", () => {
    describe("nav", () => {
      it('should have a link to the "savoir-faire" page in the second position', () => {
        cy.visit("http://preprodv2.peaks.fr/en/");

        const link = cy
          .get("nav li:nth(1)")
          .children()
          .first();

        link.should("have.text", "Our Know-How");

        link.click();
        cy.url().should("eq", "http://preprodv2.peaks.fr/en/savoir-faire/");
      });
    });
  });
  describe('"/savoir-faire" page', () => {
    const articles = () => cy.get(".blog-article");
    it("should have a list of blog articles", () => {
      articles().should("have.length.above", 0);
    });
    it('should redirect to the article when "Lire La Suite" is clicked', () => {
      articles()
        .first()
        .within(() => {
          cy
            .get(".more")
            .scrollIntoView({ duration: 500 })
            .click();
          cy
            .url()
            .should("not.eq", "http://preprodv2.peaks.fr/en/savoir-faire/");
        });
    });
  });
});
